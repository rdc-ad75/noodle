FROM python:3.9
COPY . /usr/project/
WORKDIR /usr/project/
RUN pip install -r requirements/base.txt
	
CMD ["python", "/usr/project/src/manage.py", "runserver", "0.0.0.0:8000"]