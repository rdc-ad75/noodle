cd %~dp0/..
for /f "tokens=1,2 delims==" %%A in (docker/.env) do set %%A=%%B
for /f "tokens=1,2 delims==" %%A in (docker/.env.local) do set %%A=%%B
if "%APP_ENV%"=="" SET APP_ENV=dev
for /f "tokens=1,2 delims==" %%A in (docker/.env.%APP_ENV%) do set %%A=%%B
for /f "tokens=1,2 delims==" %%A in (docker/.env.%APP_ENV%.local) do set %%A=%%B
SET http_proxy=%HTTP_PROXY%
SET https_proxy=%HTTP_PROXY%