# Docker NOODLE

## Initialisation de la plateforme

Le paramétrage par défaut de la plateforme Docker se trouve dans docker/.env

Paramètres disponibles:
* APP_ENV : mode d'exploitation de la plateforme. Par convention, ce paramètre peut avoir les valeurs dev, test ou prod
* HTTP_PROXY : adresse+port du proxy si nécessaire. Format http://<ip>:<port>
* DATABASE_USER : utilisateur de la base de donnée. Défaut : user
* DATABASE_PASSWORD : mot de passe de la base de donnée. Défaut : password
* DATABASE_NAME : nom de la base de donnée. Défaut : dbname
* DATABASE_HOST : hôte serveur de donnée. Défaut : host.docker.internal qui est l'équivalent "localhost" pour une plateforme Docker
* TAG_DATABASE : Tag Docker à utiliser pour le service "database". Défaut : postgres:13.6
* TAG_ADMINER : Tag Docker à utiliser pour le service "adminer". Défaut : adminer:4.8.1
* TAG_ORM : Tag Docker à utiliser pour le service "orm". Défaut : python:3.9
* ADMINER_HTTP_PORT : Port http à exposer pour le service Adminer. Défaut : 8080
* ORM_HTTP_PORT : Port http à exposer pour le service ORM. Défaut : 8000

Ce paramétrage par défaut peut être surchargé par les fichiers suivant dans l'ordre de chargement

* .env.local : notamment pour définir le mode d'exploitation local APP_ENV.
	* ATTENTION: si APP_ENV est surchargé côté Docker, ne pas oublier de modifier la valeur du code ORM en conséquence (orm-django/src/MAIN/local.py:APP_ENV)