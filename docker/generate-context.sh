#!/bin/sh
cd "$(dirname "$0")/.."
echo "$(pwd)"
. docker/.env
if [ -f docker/.env.local ]; then . docker/.env.local; fi
. docker/.env.${APP_ENV}
if [ -f docker/.env.${APP_ENV}.local ]; then . docker/.env.${APP_ENV}.local; fi
echo "# Contexte généré automatiquement pour l'environnement Linux" > .context
echo "export APP_ENV='${APP_ENV}'" >> .context
echo "export HTTP_PROXY='${HTTP_PROXY}'" >> .context
echo "export http_proxy='${HTTP_PROXY}'" >> .context
echo "export https_proxy='${HTTP_PROXY}'" >> .context
echo "export DATABASE_USER='${DATABASE_USER}'" >> .context
echo "export DATABASE_PASSWORD='${DATABASE_PASSWORD}'" >> .context
echo "export DATABASE_NAME='${DATABASE_NAME}'" >> .context
echo "export DATABASE_HOST='${DATABASE_HOST}'" >> .context
echo "export TAG_DATABASE='${TAG_DATABASE}'" >> .context
echo "export TAG_ADMINER='${TAG_ADMINER}'" >> .context
echo "export TAG_ORM='${TAG_ORM}'" >> .context
echo "export ADMINER_HTTP_PORT='${ADMINER_HTTP_PORT}'" >> .context
echo "export ORM_HTTP_PORT='${ORM_HTTP_PORT}'" >> .context
echo "export DATABASE_EXPOSED_PORT='${DATABASE_EXPOSED_PORT}'" >> .context
echo "export REGISTRY_IMAGE='${REGISTRY_IMAGE}'" >> .context