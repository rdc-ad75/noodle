# Utilisation de Docker Compose pour NOODLE

## Principe général

L'utilisation de Docker-Compose dans AEROBI se repose sur le mécanisme de concaténation de fichiers de paramétrage Docker-Compose offert par l'outil

Ainsi contrairement au fonctionnement standard et basique de Docker-Compose qui vient juste lire le fichier docker-compose.yml présent dans le dossier courant, il est précisé une liste de fichiers de paramétrage à lire et concaténer dans un ordre précis grâce à l'option ***-f***

Différents fichiers de paramétrages sont présents dans le dossier
Ces fichiers sont utilisés de manière conditionnelle selon l'étape (build ou up) et selon le mode d'exécution souhaité [APP_ENV=dev|test|prod] précisé dans les fichiers de configuration docker dans le répertoire parent (généralement .env.local)
* docker-compose.yml : fichier de paramétrage de base utilisé dans toutes les configurations
* docker-compose-[mode].yml : fichier de paramétrage spécifique au mode souhaité APP_ENV=dev|test|prod
* docker-compose-[mode]-build.yml : fichier de paramétrage spécifique au mode et à l'étape de construction des image "build"

Ainsi pour construire les images docker du mode test, il sera précisé dans la commande :
* docker-compose -f docker-compose.yml -f docker-compose-test-build.yml -f docker-compose-test.yml build

Ainsi pour monter/lancer les conteneur dockers du mode test, il sera précisé dans la commande :
* docker-compose -f docker-compose.yml -f docker-compose-test.yml up

## Choisir le mode d'exécution APP_ENV=dev/test/prod

Créer ou modifier le fichier docker/.env.local et préciser la valeur souhaité pour le paramètre APP_ENV

## Etape de construction : docker-compose_build(.sh/bat)

Construction des images Docker nécessaires

## Etape de lancement : docker-compose_up(.sh/bat)

Création des conteneurs Docker

## Etape d'arrêt : docker-compose_down(.sh/bat)

Arrêt des conteneurs Docker préalablement lancés