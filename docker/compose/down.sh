#!/bin/sh
cd "$(dirname "$0")/../.."
sh docker/generate-context.sh
. ./docker/.context
COMMAND="docker-compose -p ${PROJECT}-${APP_ENV}"
if [ -f docker/compose/docker-compose.yml ]; then COMMAND="$COMMAND -f docker/compose/docker-compose.yml"; fi
if [ -f docker/compose/docker-compose-${APP_ENV}.yml ]; then COMMAND="$COMMAND -f docker/compose/docker-compose-${APP_ENV}.yml"; fi
eval $COMMAND down