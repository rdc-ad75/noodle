cd %~dp0/../..
call docker/load-env.bat
SET COMMAND=docker-compose -p %PROJECT%-%APP_ENV%
if exist docker/compose/docker-compose.yml (SET COMMAND=%COMMAND% -f docker/compose/docker-compose.yml)
if exist docker/compose/docker-compose-%APP_ENV%.yml (SET COMMAND=%COMMAND% -f docker/compose/docker-compose-%APP_ENV%.yml)
%COMMAND% down || pause