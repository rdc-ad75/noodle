from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView

class IndexView(TemplateView):
    def get(self, request):
        # <view logic>
        return render(request, "noodle/index.html")
